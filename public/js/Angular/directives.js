angular.module('app').directive('ngShow', function() {
    return function(scope, elem, attrs) {
        var doShow = scope.$eval(attrs.ngShow);
        elem[doShow ? 'removeClass' : 'addClass']('ng-hide');
    };
});
angular.module('app').directive('toggle', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if (attrs.toggle == "tooltip") {
                $(element).tooltip();
            }
            if (attrs.toggle == "popover") {
                $(element).popover();
                // console.log(element, attrs);
                // $('[data-toggle="popover"]').popover();
            }
        }
    };
});
angular.module('app').directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {

            if (event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
});
