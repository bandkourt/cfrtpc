angular.module("app",
    [
        'ngAnimate',
        'ui.utils.masks',
        'idf.br-filters',
        'angularUtils.directives.dirPagination',
        'angularTreeview',
        'ngFileUpload',
        'angular-img-cropper'
    ]
);

angular.module('app').config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}]);

angular.module('app').config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('loadingInterceptor');
}]);
