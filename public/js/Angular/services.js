angular.module('app').service("urlApi", ['$location', function($location) {
    this.getURL = function() {
        var url = $location.absUrl();
        var partial = url.split("#");
        var aux = partial[0].split("/");
        var int = parseInt(aux.length);
        aux.splice(int-1, 1);
        var txt = "";

        angular.forEach(aux, function(value, key) {
            txt += value + "/";
        });

        return txt;
    };
}]);

angular.module('app').service('loadingInterceptor', ['$q','$rootScope','$log', function($q, $rootScope, $log) {
    'use strict';
    var xhrCreations = 0;
    var xhrResolutions = 0;

    function isLoading() {
        return xhrResolutions < xhrCreations;
    }

    function updateStatus() {
        $rootScope.loading = isLoading();
    }

    return {
        request: function(config) {
            xhrCreations++;
            updateStatus();

            return config;
        },
        requestError: function(rejection) {
            xhrResolutions++;
            updateStatus();
            $log.error('Request error:', rejection);

            return $q.reject(rejection);
        },
        response: function(response) {
            xhrResolutions++;
            updateStatus();

            return response;
        },
        responseError: function(rejection) {
            xhrResolutions++;
            updateStatus();
            $log.error('Response error:', rejection);

            return $q.reject(rejection);
        }
    };
}]);
