angular.module('app').service("sweetAlert", function() {
    this.alert = function(arrayAlert) {
        // var backgroundColor     = '#FFF';
        // var classColorFontTitle = 'toast-title-default';
        //
        // var arrayColorBackground = new Array();
        //
        // arrayColorBackground.push({hexadecimal_color: '#F44336', color: 'error'});
        // arrayColorBackground.push({hexadecimal_color: '#FF9800', color: 'warning'});
        // arrayColorBackground.push({hexadecimal_color: '#4CAF50', color: 'success'});
        // arrayColorBackground.push({hexadecimal_color: '#03A9F4', color: 'info'});
        // arrayColorBackground.push({hexadecimal_color: '#ffc107', color: 'question'});
        //
        // var index = arrayColorBackground.findIndex(function(x) {
        //     return x.color === arrayAlert.type;
        // });
        //
        // if (index >= 0) {
        //     backgroundColor     = arrayColorBackground[index].hexadecimal_color;
        //     classColorFontTitle = 'toast-title';
        // }

        var toast = swal.mixin( {
            toast             : true,
            position          : 'top-end',
            showConfirmButton : false,
            timer             : 3000
        });

        toast.fire( {
            type  : arrayAlert.type,
            title : arrayAlert.message
            //background : backgroundColor,
            //title      : '<span class="' + classColorFontTitle + '">' + arrayAlert.message + '</span>'

        });
    };
});
