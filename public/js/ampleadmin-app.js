$(function () {
    "use strict";
    $("#main-wrapper").AdminSettings({
        Theme: false, // this can be true or false ( true means dark and false means light ),
        Layout: 'vertical',
        LogoBg: 'skin5', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
        NavbarBg: 'skin1', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
        SidebarType: 'mini-sidebar', // You can change it full / mini-sidebar / iconbar / overlay
        SidebarColor: 'skin5', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
        SidebarPosition: true, // it can be true / false ( true means Fixed and false means absolute )
        HeaderPosition: true, // it can be true / false ( true means Fixed and false means absolute )
        BoxedLayout: false, // it can be true / false ( true means Boxed and false means Fluid )
    });
    $('.is-toggle').click(function(e) {
        if ($(e.target).is('i')) {
            if ($(e.target).parent().prop('class').indexOf('not-toggle') < 0) {
                $(this).next().toggle();
                $(this).toggleClass('arrow');
            }
        } else if (($(e.target).attr('class') && $(e.target).attr('class').indexOf('not-toggle') < 0) || !$(e.target).attr('class')) {
            $(this).next().toggle();
            $(this).toggleClass('arrow');
        }
    });

    if ($('header.is-toggle').length > 0) {
        $('header.is-toggle').each(function() {
            if ($(this).data('close')) {
                $(this).next().toggle();
                $(this).toggleClass('arrow');
            }
        });
    }

});
